import axios from "axios";

const baseApi = "https://www.balldontlie.io/api/v1";

export const balldontlieClient = (path: string) =>
  axios.create({
    baseURL: `${baseApi}${path}`,
  });
