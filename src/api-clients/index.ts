import { balldontlieClient } from "./common";

type Team = {
  id: number;
  abbreviation: string;
  city: string;
  conference: string;
  division: string;
  full_name: string;
  name: string;
};

export type Player = {
  id: number;
  first_name: string;
  height_feet: number | null;
  height_inches: number | null;
  last_name: string;
  position: string;
  team: Team;
  weight_pounds: number | null;
};

export type Meta = {
  current_page: number;
  next_page: number;
  per_page: number;
  total_count: number;
  total_pages: number;
};

export type GetPlayersOptions = {
  page?: number;
  search?: string | null;
};

export type PlayersResponse = {
  data?: Player[];
  meta: Meta;
};

const client = balldontlieClient("/players");

export const getPlayers = async (params: GetPlayersOptions) =>
  await client.get<PlayersResponse>("/", {
    params,
  });
