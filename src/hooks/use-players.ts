import {
  useQuery,
  keepPreviousData,
  type UseQueryOptions,
} from "@tanstack/react-query";
import {
  type PlayersResponse,
  type GetPlayersOptions,
  getPlayers,
} from "../api-clients";

const getQueryKey = ({ page, search }: GetPlayersOptions) =>
  ["players", page, search].filter(Boolean);

const getFetchMethod = async ({ page, search }: GetPlayersOptions) => {
  const players = await getPlayers({ page, search });

  return players?.data;
};

export const getPlayersItemOptions = ({
  page,
  search,
}: GetPlayersOptions): UseQueryOptions<PlayersResponse> => ({
  queryKey: getQueryKey({ page, search }),
  queryFn: () => getFetchMethod({ page, search }),
  placeholderData: keepPreviousData,
});

export const usePlayers = ({ page, search }: GetPlayersOptions) =>
  useQuery(getPlayersItemOptions({ page, search }));
