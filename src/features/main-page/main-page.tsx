import React, { useState } from "react";
import { isAxiosError } from "axios";
import styles from "./main-page.module.scss";
import type { Player } from "../../api-clients";
import { Favorites } from "../favorites/favorites";
import { usePlayers } from "../../hooks/use-players";
import { CurrentPlayers } from "../current-players/current-players";

export const MainPage = () => {
  const [page, setPage] = useState(1);
  const [searchTerm, setSearchTerm] = useState("");
  const [favorites, setFavorites] = useState<Player[]>();

  const { data, isLoading, isError, error } = usePlayers({
    page,
    search: searchTerm || null,
  });

  const players = data?.data;
  const meta = data?.meta;

  const handleClicked = (player: Player) =>
    setFavorites((oldFavorites) => {
      if (oldFavorites?.find(({ id }) => id === player.id)) {
        return oldFavorites;
      }

      return oldFavorites ? [...oldFavorites, player] : [player];
    });

  if (isLoading) {
    return <>Loading...</>;
  }

  if (isError) {
    if (isAxiosError(error)) {
      return <>{error.response?.status}</>;
    }

    return <>{error.message}</>;
  }

  return (
    <div className={styles.mainPageBox}>
      <CurrentPlayers
        handleClicked={handleClicked}
        handleSearchChange={(term) => setSearchTerm(term)}
        setPage={setPage}
        meta={meta}
        players={players}
      />
      <Favorites favorites={favorites} />
    </div>
  );
};
