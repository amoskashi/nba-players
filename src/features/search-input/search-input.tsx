import debouce from "lodash.debounce";
import { useMemo, useEffect } from "react";

type Props = {
  handleSearchChange: (term: string) => void;
};

export const SearchInput = ({ handleSearchChange }: Props) => {
  const handleChange = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
    handleSearchChange(target.value);
  };

  const debouncedResults = useMemo(() => {
    return debouce(handleChange, 300);
  }, []);

  useEffect(() => {
    return () => {
      debouncedResults.cancel();
    };
  });

  return (
    <input
      type="text"
      onChange={debouncedResults}
      placeholder="Search players"
    />
  );
};
