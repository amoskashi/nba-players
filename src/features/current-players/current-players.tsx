import React from "react";
import { Pagination } from "../pagination/pagination";
import styles from "./current-players.module.scss";
import type { Meta, Player } from "../../api-clients";
import { SearchInput } from "../search-input/search-input";
import { PlayersList } from "./players-list/players-list";

type Props = {
  players: Player[] | undefined;
  meta: Meta | undefined;
  setPage: React.Dispatch<React.SetStateAction<number>>;
  handleClicked: (player: Player) => void;
  handleSearchChange: (term: string) => void;
};

export const CurrentPlayers = ({
  players,
  meta,
  setPage,
  handleClicked,
  handleSearchChange,
}: Props) => {
  return (
    <div className={styles.currentPlayersBox}>
      <SearchInput handleSearchChange={handleSearchChange} />
      {players && (
        <PlayersList players={players} handleClicked={handleClicked} />
      )}
      <Pagination
        totalPages={meta?.total_pages}
        perPage={meta?.per_page}
        paginate={(page) => setPage(page)}
      />
    </div>
  );
};
