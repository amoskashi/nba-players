import React from "react";
import styles from "./players-list.module.scss";
import type { Player } from "../../../api-clients";

type Props = {
  players: Player[];
  handleClicked: (player: Player) => void;
};
export const PlayersList = ({ players, handleClicked }: Props) => {
  return (
    <ul>
      {players.map((player) => (
        <li key={player.id} className={styles.item}>
          {`${player.first_name} ${player.last_name}`}
          <button
            onClick={() => handleClicked(player)}
            className={styles.favoriteButton}
          >
            Add favorite
          </button>
        </li>
      ))}
    </ul>
  );
};
