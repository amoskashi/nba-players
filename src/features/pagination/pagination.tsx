import React from "react";
import styles from "./pagination.module.scss";

type Props = {
  totalPages?: number;
  perPage?: number;
  paginate: (page: number) => void;
};

export const Pagination = ({
  perPage = 1,
  totalPages = 1,
  paginate,
}: Props) => {
  const pageNumbers = [];

  for (let index = 1; index <= Math.ceil(totalPages / perPage); index++) {
    pageNumbers.push(index);
  }

  return (
    <nav>
      <ul className={styles.listBox}>
        {pageNumbers.map((number) => (
          <li key={number}>
            <button onClick={() => paginate(number)} type="button">
              {number}
            </button>
          </li>
        ))}
      </ul>
    </nav>
  );
};
