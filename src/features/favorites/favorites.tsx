import React, { useState } from "react";
import styles from "./favorites.module.scss";
import type { Player } from "../../api-clients";
import { FavoriteList } from "./favorite-list/favorite-list";
import { type Color, colors, ColorList } from "./color-list/color-list";

type Props = {
  favorites?: Player[];
};

export const Favorites = ({ favorites }: Props) => {
  const [color, setColor] = useState<Color>(colors[0]);

  return (
    <>
      <div
        className={styles.favoritesBox}
        style={{ backgroundColor: `rgb(var(${color}))` }}
      >
        <ColorList setColor={setColor} />
        <span>Favorites</span>
        {favorites && <FavoriteList players={favorites} />}
      </div>
    </>
  );
};
