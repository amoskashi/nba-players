import React from "react";

export const colors = ["--linen", "--azureishWhite", "--mistyRose"] as const;

export type Color = (typeof colors)[number];

type Props = {
  setColor: React.Dispatch<React.SetStateAction<Color>>;
};

export const ColorList = ({ setColor }: Props) => {
  return (
    <select
      onChange={({ target }) => {
        setColor(target.value as Color);
      }}
    >
      {colors.map((color) => (
        <option value={color} key={color}>
          {color}
        </option>
      ))}
    </select>
  );
};
