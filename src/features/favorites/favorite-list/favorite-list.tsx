import React from "react";
import styles from "./favorite-list.module.scss";
import type { Player } from "../../../api-clients";

type Props = {
  players: Player[];
};
export const FavoriteList = ({ players }: Props) => {
  return (
    <ul>
      {players.map((player) => (
        <li
          key={player.id}
          className={styles.favoriteItem}
        >{`${player.first_name} ${player.last_name}`}</li>
      ))}
    </ul>
  );
};
